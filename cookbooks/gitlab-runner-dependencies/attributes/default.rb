default[:gitlab_runner] = {
  'root_path': 'C:\GitLab-Runner',
  'exe_path': 'C:\GitLab-Runner\gitlab-runner.exe',
  'version': 'v15.8.2',
  'checksum': '0ece48eeb5f5db88e7bf812e3c8b29467461e9c46d8bbec5c2e3a69d6de21f15',
}

default[:git] = {
  'root_path': 'C:\Git',
  'zip_path': 'C:\Git\mingit.zip',
  'version': '2.23.0',
  'build': '1',
  'checksum': '8f65208f92c0b4c3ae4c0cf02d4b5f6791d539cd1a07b2df62b7116467724735',
}

default[:git_lfs] = {
  'root_path': 'C:\GitLFS',
  'zip_path': 'C:\GitLFS\lfs.zip',
  'version': 'v2.8.0',
  'checksum': 'ffeb6e0a7d214155d87fde11c4366da4f7549705f93f8873ad43ec7520282d45',
}
