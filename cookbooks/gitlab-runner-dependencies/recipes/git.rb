directory 'Create root directory' do
  path node[:git][:root_path]
  action :create
end

remote_file 'Download git from remote' do
  path node[:git][:zip_path]
  source "https://github.com/git-for-windows/git/releases/download/v#{node[:git][:version]}.windows.#{node[:git][:build]}/MinGit-#{node[:git][:version]}-64-bit.zip"
  checksum node[:git][:checksum]
  action :create
end

powershell_script 'Extract mingit.zip' do
  code "Expand-Archive -Path #{node[:git][:zip_path]} -DestinationPath #{node[:git][:root_path]}"
  cwd node[:git][:root_path]
end

file 'Delete zip archive' do
  path node[:git][:zip_path]
  action :delete
end

windows_path 'Add Git to path' do
  path "#{node[:git][:root_path]}\\cmd"
  action :add
end

